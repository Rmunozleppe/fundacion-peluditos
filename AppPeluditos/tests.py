from django.test import TestCase
import unittest
from .views import home, somos, contacto, hacemos, registro, tienda
from django.urls import reverse, resolve

# Create your tests here.

class Test_url(unittest.TestCase):
    def test_home(self):
        url = reverse('Home')
        self.assertEquals(resolve(url).func, home)

    def test_somos(self):
        url = reverse('Somos')
        self.assertEquals(resolve(url).func, somos) 

    def test_hacemos(self):
        url = reverse('Hacemos')
        self.assertEquals(resolve(url).func, hacemos) 

    def test_registro(self):
        url = reverse('Registro')
        self.assertEquals(resolve(url).func, registro)

    def test_tienda(self):
        url = reverse('Tienda')
        self.assertEquals(resolve(url).func, tienda) 

    def test_contacto(self):
        url = reverse('Contacto')
        self.assertEquals(resolve(url).func, contacto)  

     
  