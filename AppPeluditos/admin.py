from django.contrib import admin
from .models import Contacto, Producto

# Register your models here.

class ProductoAdmin(admin.ModelAdmin):
    search_fields = ["nombre"]
    list_filter = ["precio"]


admin.site.register(Contacto)
admin.site.register(Producto, ProductoAdmin)



