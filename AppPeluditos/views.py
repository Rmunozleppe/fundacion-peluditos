from django.shortcuts import render,redirect, get_object_or_404
from .forms import ContactoForm, CustomUserCreationForm, ProductoForm
from django.contrib import messages
from django.contrib.auth import authenticate,login
from django.http import Http404  
from .models import Producto
from django.db.models import Q
from rest_framework import viewsets
from .serializers import ProductoSerializer

# Create your views here.



class ProductoViewset(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer


    def get_queryset(self):
        productos = Producto.objects.all()

        nombre = self.request.GET.get('nombre')

        if nombre:
            productos = productos.filter(nombre__contains=nombre)

        return productos




def home(request):
   
    return render(request,"AppPeluditos/home.html")

def somos(request):

    return render(request,"AppPeluditos/somos.html")

def hacemos(request):

    return render(request,"AppPeluditos/hacemos.html")

def contacto(request):
    data = {
            'form': ContactoForm()
    }

    if request.method == 'POST':
        formulario = ContactoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Te contactaremos pronto!")
        else:
                data["form"] = formulario
    return render(request,"AppPeluditos/contacto.html",data)

def registro(request):
    data = {
        'form': CustomUserCreationForm()
    }

    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"] , password=formulario.cleaned_data["password1"])
            login(request, user)
            messages.success(request, "Usuario registrado correctamente")
            return redirect(to="Home")
        data["form"] = formulario

    return render(request, 'registration/registro.html', data)



def tienda(request):
    busqueda = request.GET.get("buscar")
    productos = Producto.objects.all()
    if busqueda:
        productos = Producto.objects.filter(
            Q(nombre__icontains = busqueda)
        ).distinct()
    data = {
        'productos': productos
    }

    
    
    return render(request,"AppPeluditos/tienda.html", data)

def agregar(request):

    data = {
        'form': ProductoForm()
    }

    if request.method == 'POST':
        formulario = ProductoForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Producto agregrado correctamente")
            return redirect(to="Tienda")
        else:
            data["form"] = formulario   
    return render (request,"AppPeluditos/agregar.html", data)    


def modificar(request, id):
    
    producto = get_object_or_404(Producto, id=id)

    data = {
        'form': ProductoForm(instance=producto)
    }

    if request.method == 'POST':
        formulario = ProductoForm(data=request.POST, instance=producto, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Producto modificado correctamente")
            return redirect(to="Tienda")
        data["form"] = formulario    

    return render (request, "AppPeluditos/modificar.html", data)

def eliminar(request, id):
    producto = get_object_or_404(Producto, id=id)
    producto.delete()
    messages.success(request, "Producto eliminado correctamente")
    return redirect(to="Tienda")