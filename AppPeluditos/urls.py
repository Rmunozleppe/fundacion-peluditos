from django.urls import path, include
from django.contrib.auth import views as auth_views
from AppPeluditos import views
from rest_framework import routers
from .views import ProductoViewset

router = routers.DefaultRouter()
router.register('producto',ProductoViewset)



urlpatterns = [
   
    path('',views.home, name='Home'),
    path('Somos',views.somos, name='Somos'),
    path('hacemos',views.hacemos, name='Hacemos'),
    path('contacto',views.contacto, name='Contacto'),
    path('registro/',views.registro, name='Registro'),
    path('tienda',views.tienda, name='Tienda'),
    path('agregar', views.agregar, name='Agregar'),
    path('modificar/<id>/', views.modificar, name='Modificar'), 
    path('eliminar/<id>/', views.eliminar, name='Eliminar'),

    path('reset_password/', auth_views.PasswordResetView.as_view(), name="reset_password"),

    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(), name="password_reset_don"),

    path('reset/<uid64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name="password_reset_confirm"),

    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(), name="password_reset_complete"),

    path('api/',include(router.urls)),


] 